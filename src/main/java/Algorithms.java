import java.util.Objects;

public class Algorithms {

    public static int task1(int a, int b) {
        int max = (a + b + Math.abs(a - b)) / 2;
        System.out.println(max);
        return max;
    }

    public static void task2(Integer a, Integer b) {
        if(a == null || b == null) {
                throw new ExceptionNull();
        }
        a = a - b;
        b = a + b;
        a = b - a;
        System.out.println("a: " + a);
        System.out.println("b: " + b);
    }

    public static boolean task3(String a, String b) {
        if (a.length() != b.length()) {
            System.out.println("false");
            return false;
        }
        int[] letters = new int[256];
        char[] a_array = a.toCharArray();
        for (char c : a_array) {
            letters[c]++;
        }

        for (int i = 0; i < b.length(); i++) {
            int c = b.charAt(i);
            if (--letters[c] < 0) {
                System.out.println("false");
                return false;
            }
        }
        System.out.println("true");
        return true;
    }

    public static void task4(int a) {

        if (a % 3 == 0) {
            System.out.println("foo");
        }
        if (a % 5 == 0) {
            System.out.println("bar");
        }

        if (a % 3 == 0 && a % 5 == 0) {
            System.out.println("foobar");
        }
        if (a % 3 != 0 && a % 5 != 0) {
            System.out.println(a);
        }
    }

    public static void task5(int[] a, Integer b) {
        Integer c = 0;
        Integer max = Integer.MAX_VALUE;
        for (Integer j : a) {
            if (Objects.equals(j, b)) {
                System.out.println(j);

            } else {
                Integer d = Math.abs(b - j);
                if (d.equals(max)) {
                    c = Math.max(c, j);
                }
                if (d < max) {
                    max = d;
                    c = j;
                }
            }
        }
        System.out.println(c);
    }

    public static void task6(String one) {


        String[] words = one.split(" ");
        StringBuilder phrase = new StringBuilder();
        StringBuilder reverseWord = new StringBuilder();
        for (String word : words) {
            if (word.length() >= 5) {
                for (int i = word.length() - 1; i >= 0; i--) {
                    reverseWord.append(word.charAt(i));
                }
                phrase.append(reverseWord).append(" ");
                continue;
            }
            phrase.append(word).append(" ");
        }
        System.out.println(phrase);
    }

    public static void task7(int a) {
        int x = a / 100;
        int y = a % 10;
        int z = a / 10;
        int zS = z % 10;
        int aS = x + y + zS;
        System.out.println(aS);
    }
}
