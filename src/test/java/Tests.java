import io.qameta.allure.Step;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Tests {

    @DataProvider
    public Object[][] taskOne() {
        return new Object[][] {
                {-24, -111},
                {3, 3},
                {123, 124}
        };
    }
    @Step("Задание №1")
    @Test(dataProvider = "taskOne")
    public void task1Test(int one, int two) {
        Algorithms.task1(one, two);
    }

    @DataProvider
    public Object[][] taskTwo() {
        return new Object[][] {
                {2,10},
                {123,32},
                {-67,-103},
                {2, null}
        };
    }
    @Step("Задание №2")
    @Test(dataProvider = "taskTwo")
    public void task2Test(Integer one, Integer two) {
        Algorithms.task2(one,two);
    }

    @DataProvider
    public Object[][] taskThree() {
        return new Object[][] {
                {"Qwe","qWe"},
                {"qwertsa","weqrtas"},
                {"qwert","qwer"},
        };
    }
    @Step("Задание №3")
    @Test(dataProvider = "taskThree")
    public void task3Test(String one, String two) {
        Algorithms.task3(one,two);
    }

    @DataProvider
    public Object[][] taskFour() {
        return new Object[][] {
                {3},
                {5},
                {15},
                {4},
                {48},
                {0}
        };
    }
    @Step("Задание №4")
    @Test(dataProvider = "taskFour")
    public void task4Test(int one) {
        Algorithms.task4(one);
    }

    @DataProvider
    public Object[][] taskFive() {
        return new Object[][] {
                {8,12,9,11},
                {9,30,8,12},
                {3,30,9,11},
                {3,30,10,11},
                {-8,12,-9,-7},
                {152,12,13,8}
        };
    }
    @Step("Задание №5")
    @Test(dataProvider = "taskFive")
    public void task5Test(Integer one,Integer two,Integer three,Integer four) {
        Algorithms.task5(new int[]{one,two,three,four}, 10);

    }

    @DataProvider
    public Object[][] taskSix() {
        return new Object[][]{
                {"Это новый тест"},
                {"Тут нет места"},
                {"Все работает"}
        };
    }
    @Step("Задание №6")
    @Test(dataProvider = "taskSix")
    public void task6Test(String one) {
        Algorithms.task6(one);
    }
    @DataProvider
    public Object[][] taskSeven() {
        return new Object[][]{
                {424},
                {127},
                {213}
        };
    }
    @Step("Задание №7")
    @Test(dataProvider = "taskSeven")
    public void task7Test(int one) {
        Algorithms.task7(one);
    }
}
